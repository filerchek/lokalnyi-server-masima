<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterPension extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('dishes',ChoiceType::class,array(
                'choices' => array(
                    'Да' => true,
                    'Нет' => false
                )
            ))
            ->add('mud_vuns',ChoiceType::class,array(
                'choices' => array(
                    'Да' => true,
                    'Нет' => false
                )
            ))
            ->add('massage',ChoiceType::class,array(
                'choices' => array(
                    'Да' => true,
                    'Нет' => false
                )
            ))
            ->add('Далее', SubmitType::class);

    }
}

