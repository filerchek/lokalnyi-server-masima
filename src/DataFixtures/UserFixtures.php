<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewTenant([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
            'call_name' => 'Dyadya Vasya'
        ],false);

        $manager->persist($user);

        $user2 = $this->userHandler->createNewLandlord([
            'email' => 'ewq@qwe.ru',
            'passport' => 'qwerty & some',
            'password' => 'qwerty',
            'roles' => ['ROLE_LANDLORD'],
            'full_name' => 'Vasya Dyadya'
        ],false);

        $manager->persist($user2);


        $manager->flush();
    }
}
