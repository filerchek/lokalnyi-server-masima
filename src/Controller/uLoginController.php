<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 19.06.18
 * Time: 19:48
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/add_social_id", name="app-add-social-id")
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addSocialIdAction(
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $uid = $userData['uid'];
        $network = $userData['network'];

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        try {
            $apiContext->updateClient($currentUser->getEmail(), [
                'network' => [
                    $network => $uid
                ]
            ]);

            $currentUser->setSocialId($network, $uid);
            $manager->flush();

            $this->addFlash(
                'notice',
                'Вы успешно приявязали социальную сеть к своему аккаунту'
            );
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                'Что-то пошло не так, попробуйте снова через некоторое, очень продолженное время.'
            );
        }

        return $this->redirectToRoute('app-client-profile', [
            'id' => $this->getUser()->getId()
        ]);
    }

    /**
     * @Route("/profile", name="app-client-profile")
     */
    public function profileAction(){
        $client = $this->getUser();

        return $this->render('profile/profile.html.twig', [
            'client' => $client
        ]);
    }


    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------" . time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/social-auth", name="app-social_auth")
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function socialAuth(
        UserHandler $userHandler,
        UserRepository $userRepository,
        ApiContext $apiContext,
        ObjectManager $manager
    ){
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);


        $user = $userRepository->getByUserOfUidSocial($userData['network'],$userData['uid']);


        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('app-client-profile');
        }

        try {
            if ($apiContext->socialClientExist(
                $userData['network'],
                $userData['uid']
            )) {

                $userDataSocial = $apiContext->socialClientExist($userData['network'],$userData['uid']);

                $user = $userRepository->getByUserOfUidSocial($userData['network'],$userData['uid']);

                if($user){
                    $user->setSocialId($userData['network'],$userData['uid']);
                }else{
                    $user = $userHandler->createNewUser($userDataSocial);
                }

                $manager->persist($user);
                $manager->flush();
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('app-client-profile');
            } else {
                return $this->redirectToRoute('app-sign-up');
            }
        } catch (ApiException $e) {
            echo new Response('Что то тут пошло не так');
        }

        return $this->redirectToRoute('app-client-profile');

    }
}
